pytest==4.3.0
docker[tls]==3.7.0
boto3==1.9.151
awscli==1.16.174
requests2==2.16.0
bitbucket-pipes-toolkit==1.11.0
